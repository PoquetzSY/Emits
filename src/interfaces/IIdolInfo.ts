export interface IIDolInfo {
    id: number;
    name: string;
    lastName: string;
    age: number;
    nationality: string;
    birthdate: string;
    photo: string;
    color: string;
    group: string;
    colorGroup: string;
    role: string;
}
